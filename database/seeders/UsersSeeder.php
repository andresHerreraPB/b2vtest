<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $json = File::get(base_path() . '/database/json/load_users.json');
        $data = json_decode($json);
        foreach ($data as $item){
            DB::table('users')->insert([
                'name' => $item->name,
                'email' => $item->email,
                'password' => Hash::make('B2V2022'),
                'role' => $item->role,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
