<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opciones', function (Blueprint $table) {
            $table->id('id_opcion');
            $table->string('opcion_a');
            $table->string('opcion_b');
            $table->string('opcion_c');
            $table->enum('opcion_correcta', ['opcion_a', 'opcion_b', 'opcion_c']);
            $table->unsignedBigInteger('pregunta_correspondiente');
            $table->unsignedBigInteger('test_correspondiente');
            $table->foreign('pregunta_correspondiente')->references('id_pregunta')->on('preguntas')->onDelete('cascade')->onUpdate('cascade'); 
            $table->foreign('test_correspondiente')->references('id_test')->on('tests')->onDelete('cascade')->onUpdate('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opciones');
    }
}
