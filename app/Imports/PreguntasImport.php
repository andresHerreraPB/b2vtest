<?php

namespace App\Imports;

use App\Models\Opcion;
use App\Models\Pregunta;
use App\Models\Test;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PreguntasImport implements ToCollection, WithHeadingRow
{
    private $id_test;
    private $id;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function __construct()
    {
        $this->id_test = Test::select('id_test')->orderBy('id_test', 'DESC')->pluck('id_test')->first();
    }
    
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            Pregunta::create([
                'pregunta' => $row['pregunta'],
                'id_test' => $this->id_test
            ]);

            $id_pregunta = Pregunta::select('id_pregunta')->orderBy('id_pregunta', 'DESC')->pluck('id_pregunta')->first();
            
            Opcion::create([
                'opcion_a' => $row['opcion_a'],
                'opcion_b' => $row['opcion_b'],
                'opcion_c' => $row['opcion_c'],
                'opcion_correcta' => $row['opcion_correcta'],
                'pregunta_correspondiente' => $id_pregunta,
                'test_correspondiente' => $this->id_test
            ]);
        }
    }
}
