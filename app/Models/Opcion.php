<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opcion extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $primaryKey = 'id_opcion';
    protected $table = 'opciones';

    protected $fillable = [
        'id_opcion',
        'opcion_a',
        'opcion_b',
        'opcion_c',
        'opcion_correcta',
        'pregunta_correspondiente',
        'test_correspondiente'
    ];

    public function test(){
        return $this->belongsTo(Test::class);
    }
    
    public function pregunta(){
        return $this->belongsTo(Pregunta::class, 'id_pregunta');
    }
}
