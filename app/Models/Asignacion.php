<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_asignacion';
    protected $table = 'asignaciones';

    protected $fillable = [
        'id_user',
        'id_test'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'id_user');
    }

    public function tests(){
        return $this->belongsTo(Test::class, 'id_test');
    }
}
