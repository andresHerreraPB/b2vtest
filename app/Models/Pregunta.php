<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $primaryKey = 'id_pregunta';
    protected $table = 'preguntas';

    protected $fillable = [
        'id_pregunta',
        'pregunta',
        'id_test'
    ];

    public function test(){
        return $this->belongsTo(Test::class);
    }

    public function opcion(){
        return $this->hasOne(Opcion::class, 'pregunta_correspondiente');
    }
}
