<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_test';
    protected $table = 'tests';

    protected $fillable = [
        'titulo_test'
    ];

    public function asignaciones(){
        return $this->hasMany(Asignacion::class);
    }

    public function preguntas(){
        return $this->hasMany(Pregunta::class, 'id_test');
    }

    public function opciones(){
        return $this->hasMany(Opcion::class, 'test_correspondiente');
    }
}
