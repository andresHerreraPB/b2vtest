<?php

namespace App\Http\Middleware;

use App\Models\Asignacion;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesactivatedTest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $id)
    {
        $cuestionario_contestado = (Asignacion::where('id_user', '=', Auth::user()->id)->where('id_test', '=', $id)->where('contestado', '=', false)->exists());

        if($cuestionario_contestado){
            return $next($request);
        }else{
            return redirect()->route('cuestionarios.index')->with(['error' => 'EL cuestionario al que intentas acceder ya ha sido contestado']);
        }
        
    }
}
