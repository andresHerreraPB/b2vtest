<?php

namespace App\Http\Controllers;

use App\Models\Test;
use App\Models\User;
use Illuminate\Http\Request;
use App\Imports\PreguntasImport;
use App\Models\Asignacion;
use App\Models\Opcion;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class CuestionariosController extends Controller
{
    public function index(){
        if(Auth::user()->role == 'ADMIN'){
            $asignacion = Test::all();
        }else{
            $asignacion = Asignacion::all()->where('id_user', '=', Auth::user()->id)->where('contestado', '=', false);
        }
        
        return view('admin.cuestionarios.index', compact('asignacion'));
    }

    public function show($id){
        $test = Test::find($id);
        $preguntas = Test::find($id)->preguntas;

        return view('admin.cuestionarios.show', compact('test', 'preguntas'));
    }

    public function create(){
        $users = User::all()->where('role', '=', 'USER');
        return view('admin.cuestionarios.create', compact('users'));
    }

    public function store(Request $request){
        $request->validate([
            "titulo_test" => "required|min:5|max:30",
            "excel-csv" => "required"
        ]);

        $datos = request();
        $test = new Test();
        $test->titulo_test = $datos['titulo_test'];

        $test->save();

        $id_test = Test::select('id_test')->orderBy('id_test', 'DESC')->pluck('id_test')->first();
        
        for($i=0; $i < count($request->input('users')); $i++){
            $asignacion = new Asignacion();
            $asignacion->id_user = $request->users[$i];
            $asignacion->id_test = $id_test;
            $asignacion->save();
        }

        Excel::import(new PreguntasImport, $request->file('excel-csv'));

        return redirect()->route('cuestionarios.index')->with(['creado' => 'Test registrado']);
    }

    public function success($id, Request $request){
        Asignacion::where('id_test', $id)->where('id_user', Auth::user()->id)->update(['contestado' => true]);
        return redirect()->route('cuestionarios.index')->with(['contestado' => 'Se ha contestado de forma exitosa el cuestionario']);
    }
}
