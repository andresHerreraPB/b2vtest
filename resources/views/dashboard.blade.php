@extends('layouts.navigation')

@section('content_header')
    <h1>Mi inicio:</h1>
@stop

@section('content')
    <div class="jumbotron bg-lightblue">
        <h1 class="display-4 text-center">Bienvenido {{Auth::user()->name }}</h1>
        <div class="row">
            <div class="col mx-auto w-100 text-center">
                    <a href="#" class="btn bg-olive btn-facebook"><i class="fab fa-facebook"></i></a>
                    <a href="#" class="btn bg-purple btn-twitter"><i class="fab fa-twitter"></i></a>
                    <a href="#" class="btn bg-pink btn-gplus"><i class="fab fa-google-plus"></i></a>
                    <a href="#" class="btn bg-danger btn-instagram"><i class="fab fa-instagram"></i></a>
                    <a href="#" class="btn bg-warning btn-youtube"><i class="fab fa-youtube"></i></a>
            </div>
        </div>
    </div>
    <hr class="my-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6">
                    <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Cache usada</span>
                    <span class="info-box-number">
                    10
                    <small>%</small>
                    </span>
                    </div>
                </div>
            </div>
            
            <div class="col-12 col-sm-6 col-md-6">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Usuarios</span>
                    <span class="info-box-number">41,410</span>
                    </div>
                </div>
            </div>
            
            
            <div class="clearfix hidden-md-up"></div>
            <div class="col-12 col-sm-6 col-md-6">
                    <div class="info-box mb-3">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Mercancia</span>
                    <span class="info-box-number">760</span>
                    </div>
                </div>
            </div>
            
            <div class="col-12 col-sm-6 col-md-6">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Nuevos registros</span>
                    <span class="info-box-number">2,000</span>
                    </div>   
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
@stop
