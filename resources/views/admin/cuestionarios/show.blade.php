@extends('layouts.navigation')

@section('content_header')
<h1>Cuestionario No {{$test->id_test}}</h1>
@stop

@section('content')
    <div class="container">
        <fieldset>
            <form action="{{ route('cuestionarios.success', $test->id_test) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('patch')
                @foreach ($preguntas as $pregunta)
            <div id="serialize-cuestion{{$pregunta->id_pregunta}}" class="question bg-white p-3 border-bottom">
                <div class="d-flex flex-row align-items-center question-title">
                    <i class="fa text-danger fa-search"></i>
                    <h6 class="mt-1 ml-2">
                        {{$pregunta->pregunta}}
                    </h6>
                </div>
                    @php
                        $option_row = \App\Models\Pregunta::find($pregunta->id_pregunta)->opcion;
                    @endphp
                <div class="ans ml-2">
                    <label class="radio"> <input type="radio"
                            name="{{$pregunta->id_pregunta}}" value="bueno">
                        <span>{{$option_row->opcion_a}}</span>
                    </label>
                </div>
                <div class="ans ml-2">
                    <label class="radio"> <input type="radio"
                            name="{{$pregunta->id_pregunta}}" value="bueno">
                        <span>{{$option_row->opcion_b}}</span>
                    </label>
                </div>
                <div class="ans ml-2">
                    <label class="radio"> <input type="radio"
                            name="{{$pregunta->id_pregunta}}" value="bueno">
                        <span>{{$option_row->opcion_c}}</span>
                    </label>
                </div>
            </div>
            @endforeach
            
            <button type="submit" class="btn btn-success btn-block btn-lg">
                Enviar
            </button>
        </form>
        </fieldset>
    </div>
@stop

@section('css')
<style>
    label.radio {
        cursor: pointer;
    }

    label.radio input {
        position: absolute;
        top: 0;
        left: 0;
        visibility: hidden;
        pointer-events: none;
    }

    label.radio span {
        padding: 4px 0px;
        border: 1px solid rgb(56, 34, 156);
        display: inline-block;
        color: rgb(56, 34, 156);
        width: 100px;
        text-align: center;
        border-radius: 3px;
        margin-top: 7px;
        text-transform: uppercase;
    }

    label.textarea {
        cursor: pointer;
    }

    label.textarea textarea {
        border-color: rgb(54, 110, 156);
        background-color: #A0C6F4;
        color: #fff;
        transition: 300ms;
    }

    label.textarea textarea:focus {
        border-color: rgba(54, 110, 156, 0.1);
        background-color: rgba(54, 110, 156, 0.1);
        color: rgb(54, 110, 156);
        transition: 300ms;
    }

    label.radio input:checked+span {
        border-color: rgb(54, 110, 156);
        background-color: rgb(54, 110, 156);
        color: #fff;
        transition: 300ms;
    }

    .ans {
        margin-left: 36px !important;
    }

    .btn:focus {
        outline: 0 !important;
        box-shadow: none !important;
    }

    .btn:active {
        outline: 0 !important;
        box-shadow: none !important;
    }
    .barra {
        height: 28px;
        font-size: 15px;
    }
</style>
@stop