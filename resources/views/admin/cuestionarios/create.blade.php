@extends('layouts.navigation')

@section('content_header')
    <h1>Crear cuestionario:</h1>
@stop

@section('content')
    <div class="container">
        <form enctype="multipart/form-data" method="POST" action="{{route('cuestionarios.store')}}">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-12 col-12 text-center bg-light m-2">
                  <label for="titulo_test">Titulo del test</label>
                  <input type="text" class="form-control" id="titulo_test" name="titulo_test" placeholder="Agregar titulo">
                </div>
                <div class="form-group col-md-12 col-12 text-center bg-light m-2">
                  <label for="inputPassword4">Archivo CSV</label>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="excel-csv" name="excel-csv" lang="es">
                    <label class="custom-file-label" id="excel-csv-file" for="customFileLang">Seleccionar Archivo</label>
                  </div>
                </div>
            </div>

            <hr>
            <h4>Asignar a los siguientes ususarios: </h4>
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              @foreach ($users as $user)
                <label class="btn btn-info">
                  <input type="checkbox" name="users[]" id="users" value="{{$user->id}}" autocomplete="off"> {{$user->name}}
                </label>
              @endforeach
            </div>
            <hr>
            <center>
            <button type="submit" class="btn btn-primary mx-auto my-4">Cargar archivo</button>
            </center>
         </form>
    </div>
@stop

@section('css')
@stop

@section('js')
<script type="application/javascript">
  $('#excel-csv').change(function(e){
      var fileName = e.target.files[0].name;
      $('#excel-csv-file').html(fileName);
  });
</script>
@stop