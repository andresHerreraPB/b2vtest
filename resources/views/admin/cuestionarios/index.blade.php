@extends('layouts.navigation')

@section('content_header')
<div class="row">
    <div class="col">
        <h1>Cuestionarios existentes</h1>
    </div>
    <div class="col">
        @if(Auth::user()->role == 'ADMIN')
        <a href="{{ route('cuestionarios.create') }}" class="btn bg-success float-right"
        style="color: white !important">
            Nuevo
            <i class="fa fa-plus" aria-hidden="true"></i>
        </a>
        @endif
    </div>
</div>
    
    
@stop

@section('content')
@if (count($asignacion) > 0)
    @if (Auth::user()->role == 'ADMIN')
    <div class="container">
        <div class="row">
        @foreach ($asignacion as $asig)
            <div class="col-lg-3 col-6">
                <div class="small-box bg-info">
                    <div class="inner">
                        <p>{{$asig->titulo_test}}</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-fw fa-book-open"></i>
                    </div>
                    <a href="#" class="small-box-footer">Creado el: {{$asig->created_at}}</i></a>
                </div>
            </div>
        @endforeach
        </div>
    </div>
    @else
    <div class="container">
        <div class="row">
        @foreach ($asignacion as $asig)
            <div class="col-lg-3 col-6">
                <div class="small-box bg-info">
                    <div class="inner">
                        <p>{{$asig->tests->titulo_test}}</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-fw fa-book-open"></i>
                    </div>
                    <a href="{{ route('cuestionarios.show', $asig->tests->id_test) }}" class="small-box-footer">Ver cuestionario <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        @endforeach
        </div>
    </div>
    @endif
@else
<div class="page-wrap d-flex flex-row align-items-center pt-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <span class="display-1 d-block">No se encontraron cuestionarios disponibles. ):</span>
                @if (Auth::user()->role == 'USER')
                <div class="mb-4 lead">Verifica que estes vinculado a un cuestionario.</div>
                @endif
            </div>
        </div>
    </div>
</div>
@endif
    
@stop

@section('css')
@stop

@section('js')
@if(session('creado'))
<script>
Swal.fire({
    position: 'top-end',
    icon: 'success',
    toast: true,
    backdrop: false,
    title: '{{session('creado')}}',
    showConfirmButton: false,
    timer: 2500,
    hideClass: {
            popup: 'animate__animated animate__backOutUp'
        }
  })
</script>
@elseif(session('contestado'))
<script>
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        toast: true,
        backdrop: false,
        title: '{{session('contestado')}}',
        showConfirmButton: false,
        timer: 2500,
        hideClass: {
                popup: 'animate__animated animate__backOutUp'
            }
      })
    </script>
@elseif(session('error'))
<script>
    Swal.fire({
        icon: 'warning',
        title: '{{session('error')}}',
        showConfirmButton: false,
        timer: 1500,
        hideClass: {
                popup: 'animate__animated animate__backOutUp'
            }
      });
    </script>
@endif
@stop