<?php

use App\Http\Controllers\CuestionariosController;
use Illuminate\Support\Facades\Route;

require __DIR__.'/auth.php';
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/dashboard/cuestionarios/all', [CuestionariosController::class, 'index'])->middleware(['auth'])->name('cuestionarios.index');
Route::get('/dashboard/cuestionarios/show/{id}', [CuestionariosController::class, 'show'])->middleware(['auth', 'user', 'desactivatedTest:id'])->name('cuestionarios.show');
Route::patch('/dashboard/cuestionarios/success/{id}', [CuestionariosController::class, 'success'])->middleware(['auth', 'user', 'desactivatedTest:id'])->name('cuestionarios.success');
Route::get('/dashboard/cuestionarios/create', [CuestionariosController::class, 'create'])->middleware(['auth', 'admin'])->name('cuestionarios.create');
Route::post('/dashboard/cuestionarios/create', [CuestionariosController::class, 'store'])->middleware(['auth', 'admin'])->name('cuestionarios.store');
